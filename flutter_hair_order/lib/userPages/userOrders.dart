import 'package:flutter/material.dart';
import 'package:flutter_hair_order/json/ordersJson.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<Order>> _getOrders() async {
  final data =
  await http.get("http://192.168.2.110:9000/getCompanys");
  var jsonData = json.decode(data.body);

  List<Order> orders = [];

  for (var u in jsonData) {
    Order order = Order(u["orderId"], u["orderCreateDate"], u["startTime"], u["endTime"], u["productName"], u["productPrice"], u["barberName"], u["cName"],u["type"]);
    orders.add(order);
  }
  print(orders.length);
  return orders;
}

//Future<List<Order>> _getOrders() async {
//  final data =
//  await http.get("http://192.168.1.3:9000/createOrder");
//  var jsonData = json.decode(data.body);
//
//  List<Order> orders = [];
//
//  for (var ord in jsonData) {
//    Order order = Order(ord["id"], ord["createDate"], ord["endTime"], ord["type"], ord["company_id"], ord["barber_id"], ord["product_id"], ord["service_name"]);
//    orders.add(order);
//  }
//  print(orders.length);
//  return orders;
//}

class userOrders extends StatefulWidget{
  @override
  userOrdersState createState() => new userOrdersState();
}

class userOrdersState extends State<userOrders>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Миний захиалгууд'),
        backgroundColor: Colors.deepOrange,
        elevation: 0.0,
      ),
     body: FutureBuilder(
         future: _getOrders(),
         builder: (BuildContext context, AsyncSnapshot snapshot)
     {
       if (snapshot.data == null ) {
         return Container(
           child: Text(" Танд одоогоор захиалга алга"),
         );
       }
       else {
        return ListView.builder(  itemCount: snapshot.data.length,itemBuilder: (BuildContext context, int index){
          return Container(
            margin: const EdgeInsets.only( top: 30.0),

            height: 250,
            //color:  Colors.purple[50],
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[
                new Container(

                  height: 50.0,
                  child: new Text(snapshot.data[index].productName, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24.0),)
                ),
                new Column(

                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                          color: Colors.deepPurple[50],
                          height: 30.0,
                            width: 200,
                            child: new Text("Салон", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),

                        new Container(
                            color: Colors.deepPurple[50],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].cName, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),

                      ],
                    )
                  ],

                ),

                new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                            color: Colors.lightBlue[100],
                            height: 30.0,
                            width: 200,
                            child: new Text("Үсчин", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                        //SizedBox(width: 10.0,),
                        new Container(
                            color: Colors.lightBlue[100],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].cName, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                      ],
                    )
                  ],

                ),
                new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                            color: Colors.orangeAccent[100],
                            height: 30.0,
                            width: 200,
                            child: new Text("Өдөр", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                        //SizedBox(width: 10.0,),
                        new Container(
                            color: Colors.orangeAccent[100],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].orderCreateDate, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                      ],
                    )
                  ],

                ),
                new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                            color: Colors.tealAccent[100],
                            height: 30.0,
                            width: 200,
                            child: new Text("Эхлэх цаг", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                        //SizedBox(width: 10.0,),
                        new Container(
                            color: Colors.tealAccent[100],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].startTime, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                      ],
                    )
                  ],

                ),
                new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                            color: Colors.yellow[200],
                            height: 30.0,
                            width: 200,
                            child: new Text("Дуусах цаг", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                        //SizedBox(width: 5.0,),
                        new Container(
                            color: Colors.yellow[200],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].endTime, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                      ],
                    )
                  ],

                ),
                new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                            color: Colors.pink[100],
                            height: 30.0,
                            width: 200,
                            child: new Text("Үйлчилгээний үнэ", style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                        //SizedBox(width: 10.0,),
                        new Container(
                            color: Colors.pink[100],
                            height: 30.0,
                            width: 200,
                            child: new Text(snapshot.data[index].productPrice, style: TextStyle(fontStyle: FontStyle.normal, fontSize: 20.0),)
                        ),
                      ],
                    )
                  ],

                )

              ],
            ),
          );

        });
       }
     }


     )
    );
  }
}