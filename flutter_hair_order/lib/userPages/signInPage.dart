import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/homePages/homeScreen.dart';
import 'package:flutter_hair_order/homePages/salonListPage.dart';
import 'package:flutter_hair_order/userPages/singUpPage.dart';
import 'package:http/http.dart' as http;
//import 'package:flutter_app/productPages/salonProduct.dart';

class User {
  final String username;
  final String password;

  User({this.username, this.password});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;

    return map;
  }
}

createPost(String url, {Map body}) async {
  return http.post(url, body: json.encode(body)).then((http.Response response) {
    Map data;
    final int statusCode = response.statusCode;
    print(statusCode);
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    data = json.decode(response.body);
    print(data["success"]);
    return data["success"];
  });
}

class SignInPage extends StatelessWidget {
  //static final CREATE_POST_URL = 'http://192.168.0.104:9000/loginAPI';
  TextEditingController titleControler = new TextEditingController();
  TextEditingController bodyControler = new TextEditingController();
  static final CREATE_POST_URL = 'http://192.168.2.110:9000/loginAPI';
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "WEB SERVICE",
      theme: ThemeData(
        primaryColor: Colors.deepOrange,
      ),
      home: Scaffold(

        appBar: AppBar(
          title: Text('Нэвтрэх'),
          leading: IconButton(icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, MaterialPageRoute(builder: (context)=> HomeScreen(),));
    },
        ),
        ),

        body: new Container(



          width: MediaQuery
              .of(context)
              .size
              .width,
          height: MediaQuery
              .of(context)
              .size
              .height,

          margin: const EdgeInsets.only(left: 8.0, right: 8.0, top: 100.0),
          child: new Column(


            //child: Container(
            children: <Widget>[
              new TextField(

                controller: titleControler,

                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.account_circle),
                    hintText: "Нэвтрэх нэр", labelText: 'Нэвтрэх нэр'),
              ),
              SizedBox(height: 30.0,),
              new TextField(
                controller: bodyControler,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.lock_open),
                    hintText: "Нууц үг", labelText: 'Нууц үг'),
              ),
              SizedBox(height: 30.0,),
              Container(
                width: 400,
                child:
                RaisedButton(
                  onPressed: () async {
                    User newPost = new User(
                        username: titleControler.text,
                        password: bodyControler.text
                    );
                    var body = await createPost(CREATE_POST_URL, body:newPost.toMap());
                    if( body = true){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ListSearch()),);
                    }

                  },
                  color: Colors.blue,textColor: Colors.black,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Text('Нэвтрэх', style: TextStyle(fontSize: 16.0),),
                ),
              ),
              SizedBox(height: 20.0,),
              new Column(
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Container(
                        height: 30.0,
                        width: 230,
                        child: Text("Бүртгэтгүй бол бүртгүүлнэ үү!", style: TextStyle(fontSize: 16.0, fontStyle: FontStyle.italic, color: Colors.red),),
                      ),
                      SizedBox(width: 50.0),
                      new Container(
                        height: 30.0,

                        child: RaisedButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage()),);
                        },
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                          child: Text('Бүртгүүлэх'),
                          color: Colors.deepOrange[200],
                        ),
                      )
//                    new   Container(
//                    width:180,
//                      child:
//                RaisedButton(
//                  onPressed: ()
//                  {
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage()),);
//                  },
//                  color: Colors.cyan, textColor: Colors.black,
//                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
//                  child: Text('Бүртгүүлэх'),
//                ),
//              ),
                    ],
                  )
                ],
              )
//

//              new RaisedButton(
//                onPressed: () async {
//                  User newPost = new User(
//                      username: titleControler.text,
//                      password: bodyControler.text);
//                  var body =
//                  await createPost(CREATE_POST_URL, body: newPost.toMap());
//                  if (body == true) {
//                    Navigator.push(context,
//                      MaterialPageRoute(builder: (context) => ListSearch()),);
//                    //SalonProducts();
//                  }
//                  Text("Нэвтрэх");
//
//                }
//              ),
//              new RaisedButton(
//              onPressed: () async {Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage()),
//
//
//              );
//              }
//
//              )
            ],

          ),
        ),
      ),
    );
  }
}

//void main() => runApp(MyApp());
//
//class MyApp extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return MaterialApp(
//      title: "Hair Order",
//      home: MyApp1(),
//      //HomePage(title: 'Users'),
//      debugShowCheckedModeBanner: false,
//    );
//  }
//}