/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:folding_cell/folding_cell.dart';
import 'package:flutter_hair_order/orderPages/orderHome.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ServiceDetailsMen {
  final int ProductId;
  final String ProductName;
  final int ProductPrice;
  final int ProductCompany;
  final int ProductType;
  final int ProductDuraction;
  final int ProductCategoryId;

  ServiceDetailsMen(
      this.ProductId,
      this.ProductName,
      this.ProductPrice,
      this.ProductCompany,
      this.ProductType,
      this.ProductDuraction,
      this.ProductCategoryId);
}

class ServiceDetailsWomen {
  final int ProductId;
  final String ProductName;
  final int ProductPrice;
  final int ProductCompany;
  final int ProductType;
  final int ProductDuraction;
  final int ProductCategoryId;

  ServiceDetailsWomen(
      this.ProductId,
      this.ProductName,
      this.ProductPrice,
      this.ProductCompany,
      this.ProductType,
      this.ProductDuraction,
      this.ProductCategoryId);
}

/*
*  ServiceType -> _ServiceTypeState - Үйлчилгээ харах, сонгох хэсгийн үндсэн хуудсыг дууддаг класс
*/
class ServiceList extends StatefulWidget {
  final int ServiceTypeID;
  final String ServiceName;

  ServiceList({this.ServiceTypeID, this.ServiceName});

  @override
  _ServiceListState createState() =>
      _ServiceListState(ServiceTypeID: ServiceTypeID, ServiceName: ServiceName);
}

class _ServiceListState extends State<ServiceList> {
  final int ServiceTypeID;
  final String ServiceName;
  final _foldingCellKey = GlobalKey<SimpleFoldingCellState>();

  _ServiceListState({
    this.ServiceName,
    this.ServiceTypeID,
  });

  Future<List<ServiceDetailsMen>> getServiceDetailsMen() async {
    print(ServiceTypeID);
    final data = await http
        .get("http://192.168.2.106:9000/getProducts/${ServiceTypeID}&0");

    var jsonData = json.decode(data.body);
    print(jsonData[1]['ProductName']);

    List<ServiceDetailsMen> services = [];

    for (var u in jsonData) {
      ServiceDetailsMen asd = ServiceDetailsMen(
          u["ProductId"],
          u["ProductName"],
          u["ProductPrice"],
          u["ProductCompany"],
          u["ProductType"],
          u["ProductDuraction"],
          u["ProductCategoryId"]);
      services.add(asd);
    }
    return services;
  }

  Future<List<ServiceDetailsWomen>> getServiceDetailsWomen() async {
    final data = await http
        .get("http://192.168.2.106:9000/getProducts/${ServiceTypeID}&1");

    var jsonData = json.decode(data.body);

    List<ServiceDetailsWomen> servicesWomen = [];

    for (var u in jsonData) {
      ServiceDetailsWomen women = ServiceDetailsWomen(
          u["ProductId"],
          u["ProductName"],
          u["ProductPrice"],
          u["ProductCompany"],
          u["ProductType"],
          u["ProductDuraction"],
          u["ProductCategoryId"]);
      servicesWomen.add(women);
    }
    return servicesWomen;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Text("Эрэгтэй"),
                ),
                Tab(
                  child: Text("Эмэгтэй"),
                ),
              ],
            ),
            title: Text(ServiceName),
          ),
          body: TabBarView(
            children: [
              Container(
                child: FutureBuilder(
                  future: getServiceDetailsMen(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.data == null) {
                      return Container(
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(backgroundColor: Colors.deepOrange,),
                          ],
                        )),
                      );
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                OrderHome(
                                  ServiceName: snapshot.data[index].ProductName,
                                  ServiceDuraction: snapshot.data[index].ProductDuraction,
                                ))),
                            child: Container(
                              height: 60.0,
                              width: 60.0,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 5.0),
                              child: Card(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            snapshot.data[index].ProductName,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }
                  },
                ),
              ),
              Container(
                child: FutureBuilder(
                  future: getServiceDetailsWomen(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.data == null) {
                      return Container(
                        child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircularProgressIndicator(backgroundColor: Colors.deepOrange,),
                              ],
                            )),
                      );
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () => Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        OrderHome(
                                          ServiceName: snapshot.data[index].ProductName,
                                            ServiceDuraction: snapshot.data[index].ProductDuraction,
                                        ))),
                            child: Container(
                              height: 60.0,
                              width: 60.0,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 5.0),
                              child: Card(
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            snapshot.data[index].ProductName,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
