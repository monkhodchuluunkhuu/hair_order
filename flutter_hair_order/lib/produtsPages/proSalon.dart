/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/produtsPages/proSalonDetails.dart';

/*
*  Products -> _ProductsState - Сүүлд нэмэгдсэн бүтээгдэхүүнийг харах хэсгийн жагсаалтыг дууддаг классууд
*/
class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState(); // changed
}

class _ProductsState extends State<Products> {

  // Сүүлд нэмэгдсэн бүтээгдэхүүнд юу юу байхыг агуулсан массив
  var product_list = [
    {
      "name": "Толь",
      "picture": "image/salonProduct/pro_mirror.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үсний вакус",
      "picture": "image/salonProduct/pro_budag.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үсний сэнс",
      "picture": "image/salonProduct/pro_hairfsf.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үсний угаалтуур",
      "picture": "image/salonProduct/pro_hairwash.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үчний шампони",
      "picture": "image/salonProduct/pro_shampon.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үчний тос",
      "picture": "image/salonProduct/pro_tos.jpeg",
      "old_price": 120,
      "price": 80,
    }
  ];

  /*
    @return -> GridView.builder буцаана(Энэ GridView.builder нь single_pro гэсэн классыг буцаана.
              single_pro классыг буцаахдаа дотор нь pro_name, pro_picture, pro_old_price, pro_price гэсэн утгыг дамжуулна).
   */
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index){
          return single_pro(
            pro_name: product_list[index]['name'],
            pro_picture: product_list[index]['picture'],
            pro_old_price: product_list[index]['old_price'],
            pro_price: product_list[index]['price'],
          );
        });
  }
}

/*
*  single_pro - Сүүлд нэмэгдсэн бүтээгдэхүүний жагсаалтын нэг хэсгийн харагдацыг дууддаг класс
*  @param - pro_name(С.Н бүтээгдэхүүний нэр), pro_picture(С.Н бүтээгдэхүүний зураг),
*           pro_old_price(С.Н бүтээгдэхүүний хямдарсан үнэ), pro_price(С.Н бүтээгдэхүүний одоогийн үнэ) гэсэн 4 утга авна.
*/
class single_pro extends StatelessWidget {
  final pro_name;
  final pro_picture;
  final pro_old_price;
  final pro_price;

  single_pro({
    this.pro_name,
    this.pro_picture,
    this.pro_old_price,
    this.pro_price
  });

  // @return -> Card буцаана(Энэ Card нь сүүлд нэмэгдсэн бүтээгдэхүүний жагсаалтын нэг хэсгийн харагдацыг агуулсан).
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: pro_name,
          child: Material(
            child: InkWell(
              splashColor: Colors.deepOrange,
              // Тус Card дээр дарахад сүүлд нэмэгдсэн бүтээгдэхүүний дэлгэрэнгүйг агуулсан хуудас руу үсэрнэ(ProductDetails).
              onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => ProductDetails(
                    product_detial_name: pro_name,
                    product_detial_new_price: pro_price,
                    product_detial_old_price: pro_old_price,
                    product_detial_picture: pro_picture,
                  ))),
              child: GridTile(
                  footer: Container(
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text(pro_name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),),
                        ),
                        Text("\$${pro_price}", style: TextStyle(color: Colors.deepOrange, fontWeight: FontWeight.bold ),)
                      ],
                    ),
                  ),
                  child: Image.asset(
                    pro_picture,
                    fit: BoxFit.cover,
                  )),
            ),
          )),
    );
  }
}

