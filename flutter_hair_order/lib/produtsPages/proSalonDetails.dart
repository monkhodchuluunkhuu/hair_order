/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/produtsPages/proSalonHome.dart';

/*
*  ProductDetails -> _ProductDetailsState - Сүүлд нэмэгдсэн бүтээгдэхүүний дэлгэрэнгүйг(СНБД) харуулах классууд
*  @param - product_detial_name(С.Н.Б дэлгэрэнгүй нэр), product_detial_new_price(С.Н.Б дэлгэрэнгүй одоогийн үнэ),
*           product_detial_old_price(С.Н.Б дэлгэрэнгүй хямдарсан үнэ), product_detial_picture(С.Н.Б дэлгэрэнгүй зураг) гэсэн 4 утга авна.
*/
class ProductDetails extends StatefulWidget {
  final product_detial_name;
  final product_detial_new_price;
  final product_detial_old_price;
  final product_detial_picture;

  ProductDetails(
      {this.product_detial_name,
        this.product_detial_new_price,
        this.product_detial_old_price,
        this.product_detial_picture});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  /*
  * @return -> Scaffold буцаана(Энэ Scaffold нь сүүлд нэмэгдсэн бүтээгдэхүүний дэлгэрэнгүйг бүхэлд нь харуулана).
  */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.deepOrange,
        title: InkWell(
          // Тус Text дээр дарахад Бараа бүтээгдэхүүнийг агуулсан хуудас руу үсэрнэ(SalonProducts).
            onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => SalonProducts())),
            child: Text(widget.product_detial_name,
                style: TextStyle(color: Colors.white))
        ),
      ),
      body: ListView(
        children: <Widget>[
          // СНБД хуудсын толгойн хэсэг
          Container(
            height: 250.0,
            child: GridTile(
              child: Container(
                color: Colors.white,
                child: Image.asset(widget.product_detial_picture),
              ),
              footer: Container(
                color: Colors.white70,
                child: ListTile(
                  leading: Text(
                    widget.product_detial_name,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                  title: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "\$${widget.product_detial_old_price}",
                          style: TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.lineThrough),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "\$${widget.product_detial_new_price}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.deepOrange),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),

          // СНБД хуудсын бүтээгдэхүүний хэмжээ, өнгө, тоо ширхэгтэй хэсэг
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: () => {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Хэмжээ"),
                            content: Text("Хэмжээгээ сонгоно уу?"),
                            actions: <Widget>[
                              MaterialButton(
                                onPressed: () =>
                                {Navigator.of(context).pop(context)},
                                child: Text("хаах"),
                              ),
                            ],
                          );
                        })
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  elevation: 0.2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Хэмжээ"),
                      ),
                      Expanded(
                        child: Icon(Icons.arrow_drop_down),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: MaterialButton(
                  onPressed: () => {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Өнгө"),
                            content: Text("Өнгөө сонгоно уу?"),
                            actions: <Widget>[
                              MaterialButton(
                                onPressed: () =>
                                {Navigator.of(context).pop(context)},
                                child: Text("хаах"),
                              ),
                            ],
                          );
                        })
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  elevation: 0.2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Өнгө"),
                      ),
                      Expanded(
                        child: Icon(Icons.arrow_drop_down),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: MaterialButton(
                  onPressed: () => {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text("Тоо ширхэг"),
                            content: Text("Тоо ширхэгээ сонгоно уу?"),
                            actions: <Widget>[
                              MaterialButton(
                                onPressed: () =>
                                {Navigator.of(context).pop(context)},
                                child: Text("хаах"),
                              ),
                            ],
                          );
                        })
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  elevation: 0.2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Тоо ширхэг"),
                      ),
                      Expanded(
                        child: Icon(Icons.arrow_drop_down),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),

          // СНБД хуудсын бүтээгдэхүүнийг худалдаж авах хэсэг
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: () => {},
                  color: Colors.deepOrange,
                  textColor: Colors.white,
                  elevation: 0.2,
                  child: Text("Худалдаж авах"),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.add_shopping_cart,
                  color: Colors.deepOrange,
                ),
                onPressed: () => {},
              ),
              IconButton(
                icon: Icon(
                  Icons.favorite_border,
                  color: Colors.deepOrange,
                ),
                onPressed: () => {},
              )
            ],
          ),
          Divider(
            color: Colors.deepOrange,
          ),

          // СНБД хуудсын бүтээгдэхүүний тайлбар
          ListTile(
            title: Text("Бүтээгдэхүүний тайлбар: "),
            subtitle: Text(
                " Москвагийн Николоямская гудамжинд байрлах “Ангара спасения” хэмээх гоо сайхны газарт тэнэмэл хүмүүс усанд орж, шаардлагатай нийгмийн халамжийн тусламж авах боломжтой болгох бөгөөд, өнөөдрийн байдлаар гэхэд л тэнэмэл хүмүүсийн дунд үсчнээр цалингүй ажиллах хүсэлтэй нэлээд хүн саналаа ирүүлсэн хэмээн тус улсын “Милосердие” буюу “Нинжин сэтгэл”  ТТБ-ын дарга Анна Овсяникова хэллээ. Өдөр бүр 10 мянган тэнэмэл хүн үсээ засуулахаар ханддаг гэнэ."),
          ),
          Divider(
            color: Colors.deepOrange,
          ),

          // СНБД хуудсын бүтээгдэхүүний нэр, брэнд, нөхцөлтэй хэсэг
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Бүтээгдэхүүний нэр: ",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(widget.product_detial_name),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Бүтээгдэхүүний брэнд: ",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text("Matrix"),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Бүтээгдэхүүний нөхцөл: ",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text("Шинэ"),
              )
            ],
          ),
          Divider(
            color: Colors.deepOrange,
          ),

          // СНБД хуудсын бүтээгдэхүүний ижил төстэй бүтээгдэхүүний хэсэг
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(" Ижил төстэй бүтээгдэхүүн ")),

          Container(
            height: 340.0,
            child: Similar_products(),
          ),
        ],
      ),
    );
  }
}

/*
*  Similar_products -> _Similar_productsState - Ижил төстэй бүтээгдэхүүнүүдийг харуулсан классууд
*/
class Similar_products extends StatefulWidget {
  @override
  _Similar_productsState createState() => _Similar_productsState();
}

class _Similar_productsState extends State<Similar_products> {

  // жил төстэй бүтээгдэхүүнд юу юу байхыг агуулсан массив
  var product_list = [
    {
      "name": "Үсний вакус",
      "picture": "image/salonProduct/pro_budag.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үсний сэнс",
      "picture": "image/salonProduct/pro_hairfsf.jpg",
      "old_price": 120,
      "price": 80,
    },
    {
      "name": "Үсний угаалтуур",
      "picture": "image/salonProduct/pro_hairwash.jpg",
      "old_price": 120,
      "price": 80,
    },
  ];

  /*
  * @return -> GridView.builder буцаана(Энэ GridView.builder нь Similar_single_pro гэсэн классыг буцаана.
              Similar_single_pro классыг буцаахдаа дотор нь pro_name, pro_picture, pro_old_price, pro_price гэсэн утгыг дамжуулна).
  */
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_list.length,
        gridDelegate:
        SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return Similar_single_pro(
            pro_name: product_list[index]['name'],
            pro_picture: product_list[index]['picture'],
            pro_old_price: product_list[index]['old_price'],
            pro_price: product_list[index]['price'],
          );
        });
  }
}

/*
*  Similar_single_pro - Ижил төстэй бүтээгдэхүүний жагсаалтын нэг хэсгийн харагдацыг дууддаг класс
*  @param - pro_name(Ижил төстэй бүтээгдэхүүний нэр), pro_picture( Ижил төстэй бүтээгдэхүүний зураг),
*           pro_old_price( Ижил төстэй бүтээгдэхүүний хямдарсан үнэ), pro_price( Ижил төстэй бүтээгдэхүүний одоогийн үнэ) гэсэн 4 утга авна.
*/
class Similar_single_pro extends StatelessWidget {
  final pro_name;
  final pro_picture;
  final pro_old_price;
  final pro_price;

  Similar_single_pro(
      {this.pro_name, this.pro_picture, this.pro_old_price, this.pro_price});

  // @return -> Card буцаана(Энэ Card нь ижил төстэй бүтээгдэхүүний жагсаалтын нэг хэсгийн харагдацыг агуулсан).
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: pro_name,
          child: Material(
            child: InkWell(
              // Тус Card дээр дарахад ижил төстэй бүтээгдэхүүний дэлгэрэнгүйг агуулсан хуудас руу үсэрнэ(ProductDetails).
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => ProductDetails(
                    product_detial_name: pro_name,
                    product_detial_new_price: pro_price,
                    product_detial_old_price: pro_old_price,
                    product_detial_picture: pro_picture,
                  ))),
              child: GridTile(
                  footer: Container(
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            pro_name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16.0),
                          ),
                        ),
                        Text(
                          "\$${pro_price}",
                          style: TextStyle(
                              color: Colors.deepOrange,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                  child: Image.asset(
                    pro_picture,
                    fit: BoxFit.cover,
                  )),
            ),
          )),
    );
  }
}
