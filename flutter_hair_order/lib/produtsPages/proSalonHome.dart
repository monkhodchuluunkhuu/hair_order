/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_hair_order/produtsPages/proSalon.dart';
import 'package:flutter_hair_order/produtsPages/proCategory.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';


/*
*  SalonProducts -> _SalonProductsState - Бараа бүтээгдэхүүн харах хэсгийн үндсэн хуудсыг дууддаг класс
*/
class SalonProducts extends StatefulWidget {
  @override
  _SalonProductsState createState() => _SalonProductsState();
}

class _SalonProductsState extends State<SalonProducts> {
  @override
  Widget build(BuildContext context) {
    /*
    *  image_carousel - Бараа бүтээгдэхүүн харах дэлгэцийн толгой хэсгийн зурагнуудыг харууладаг функц
    */
    Widget image_carousel = new Container(
      height: 200.0,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('image/Salon1.png'),
          AssetImage('image/Salon3.jpg'),
          AssetImage('image/header2.jpg'),
        ],
        autoplay: false,
        dotSize: 4.0,
        dotColor: Colors.deepOrange,
        dotBgColor: Colors.transparent,
        indicatorBgPadding: 3.0,
      ),
    );

    return Scaffold(
      body: ListView(
        children: <Widget>[
          //Толгой хэсгийн зураг
          image_carousel,

          Padding(padding: const EdgeInsets.all(8.0),
            child: Text('Ангилал', style: TextStyle(fontWeight: FontWeight.bold),),),

          //Бүтээгдэхүүний ангилалын харагдах байдал
          ProductList(),

          Padding(padding: const EdgeInsets.all(20.0),
            child: Text('Сүүлд нэмэгдсэн бүтээгдэхүүн', style: TextStyle(fontWeight: FontWeight.bold),),),

          //grid view
          Container(
            height: 320.0,
            child: Products(),
          ),
        ],
      ),

    );
  }
}
