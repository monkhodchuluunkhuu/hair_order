/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_hair_order/orderPages/orderNotifcationDialog.dart';

class Barber {
  final int barberId;
  final String barberUsername;
  final String barberFirstName;
  final String barberLastName;
  final String barberPhoto;

  Barber(this.barberId, this.barberUsername, this.barberFirstName, this.barberLastName, this.barberPhoto);
}

Future<List<Barber>> _getCategory() async {
  final data = await http.get("http://192.168.2.106:9000/getUschin");

  var jsonData = json.decode(data.body);

 // print(jsonData[2]["barberFirstName"]);
  List<Barber> uschins = [];

  for (var u in jsonData) {
    Barber user =
    Barber(u["barberId"], u["barberUsername"], u["barberFirstName"], u["barberLastName"], u["barberPhoto"]);
    uschins.add(user);
  }
  print(uschins);
  return uschins;
}

/*
*  BarberList -> _BarberListState - Үсчний жагсаалтын хуудсыг харуулсан классууд
*/
class OrderBarberList extends StatefulWidget {

  final int ServiceDuraction;
  final String ServiceName;

  //  @param - orderPages -> serviceName.dart-аас үйлчилгээний нэрийг авна.
  OrderBarberList({
    this.ServiceDuraction,
    this.ServiceName
  });
  @override
  _OrderBarberListState createState() => _OrderBarberListState(pro_type: ServiceDuraction, ServiceName: ServiceName); // changed
}

class _OrderBarberListState extends State<OrderBarberList> {

  DateTime selectedDate = DateTime.now();
  final int pro_type;
  final String ServiceName;
  _OrderBarberListState({
    this.pro_type,
    this.ServiceName
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: _getCategory(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          print("end orj ir");
          if (snapshot.data == null) {
            return Container(
              child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(backgroundColor: Colors.deepOrange,),
                    ],
                  )),
            );
          } else {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                print("bsdjkfnksdnf");
                return Container(
                  child: Card(
                    child: Hero(
                      tag: snapshot.data[index].barberFirstName,
                      child: Material(
                        child: InkWell(
                          onTap: () async {
                            showDateTimeDialog(
                                context,
                                initialDate: selectedDate,
                                Type: pro_type,
                                ServiceName: ServiceName,
                                BarberId: snapshot.data[index].barberId,
                                onSelectedDate: (selectedDate) {
                                  setState(() {
                                    this.selectedDate = selectedDate;
                                  });
                                });
                          },
                          child: Container(
                            height: 60.0,
                            color: Colors.deepOrange,
                            child: Column(
                              children: <Widget>[
                              Text(
                                  snapshot.data[index].barberFirstName,
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.white),
                                ),
                            Image.asset(
                               "",
                                fit: BoxFit.cover,
                              )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
