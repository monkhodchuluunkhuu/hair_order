class Order{
  int orderId;
  DateTime orderCreateDate;
  DateTime startTime;
  DateTime endTime;
  String productName;
  double productPrice;
  String barberName;
  String cName;
  int type;


  Order(this.orderId, this.orderCreateDate, this.startTime, this.endTime, this.productName, this.productPrice, this.barberName, this.cName, this.type);

}