import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/homePages/salonPage.dart';
import 'package:flutter_hair_order/userPages/userOrders.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;





class ListSearch extends StatefulWidget {
  @override
  _listSearchState createState() => new _listSearchState();

}

class _listSearchState extends State<ListSearch> {
  TextEditingController controller = new TextEditingController();

  // Get json result and convert it to model. Then add
  Future<Null> getUserDetails() async {
    final response = await http.get(url);
    final responseJson = json.decode(response.body);

    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
      }
    });
  }

  @override
  void initState() {
    super.initState();

    getUserDetails();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Салон жагсаалт'),
        backgroundColor: Colors.deepOrange,
        elevation: 0.0,
        actions: <Widget>[
      FlatButton.icon(onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => userOrders()),);},
                      icon: Icon(Icons.add_shopping_cart), label: new Text("Миний захиалга")),
        ],

      ),
      body: new Column(
        children: <Widget>[
          new Container(


            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Card(
                child: new ListTile(
                  leading: new Icon(Icons.search),
                  title: new TextField(
                    controller: controller,
                    decoration: new InputDecoration(
                        hintText: 'Search', border: InputBorder.none),
                    onChanged: onSearchTextChanged,
                  ),
//                  subtitle: FlatButton.icon(onPressed: () {
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => userOrders()),);},
//                      icon: Icon(Icons.account_circle), label: new Text("Миний бүртгэл")),
                  trailing: new IconButton(icon: new Icon(Icons.cancel), onPressed: () {
                    controller.clear();
                    onSearchTextChanged('');
                  },),
                ),
              ),

            ),
          ),
          new Expanded(
            child: _searchResult.length != 0 || controller.text.isNotEmpty
                ? new ListView.builder(
              itemCount: _searchResult.length,
              itemBuilder: (context, i) {
                return new Card(
                  child: new ListTile(
                    onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => SalonHomePage(
                          salon_image: _searchResult[i].zurag,
                          salon_name: _searchResult[i].cName,
                          salon_address: _searchResult[i].hayg,
                          salon_phone: _searchResult[i].utas,
                        ))),
                    leading: new CircleAvatar(backgroundImage: new NetworkImage(_searchResult[i].zurag),),
                    title: new Text(_searchResult[i].cName + ' ' + _searchResult[i].hayg +''+ _searchResult[i].utas ),
                  ),
                  margin: const EdgeInsets.all(0.0),
                );
              },
            )
                : new ListView.builder(
              itemCount: _userDetails.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => SalonHomePage(
                        salon_image: _userDetails[index].zurag,
                        salon_name: _userDetails[index].cName,
                        salon_address: _userDetails[index].hayg,
                        salon_phone: _userDetails[index].utas,
                      ))),
                  child: new Card(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          width: 400,
                          height: 200,
                          decoration: BoxDecoration(
                            image: new DecorationImage(image: new NetworkImage(_userDetails[index].zurag),
                                fit: BoxFit.cover
                            ),
                          ),
                        ),
                        new Column(
                          children: <Widget>[
                            new Row(

                              children: <Widget>[
                                SizedBox(height: 10.0),
                                new Container(
                                  width: 150.0,
                                  height: 50.0,
                                  child: new Text(_userDetails[index].cName, style: TextStyle(fontSize: 14.0, color: Colors.black, fontStyle: FontStyle.normal),),
                                ),
                                SizedBox(width: 20.0,),
                                new Row(
                                  children: <Widget>[
                                    new Column(
                                      children: <Widget>[
                                        new Text(_userDetails[index].hayg, style: TextStyle(fontSize: 14.0, color: Colors.black, fontStyle: FontStyle.normal),),
                                        SizedBox(height: 5.0,),
                                        new Text(_userDetails[index].utas, style: TextStyle(fontSize: 14.0, color: Colors.black, fontStyle: FontStyle.normal),),

                                      ],
                                    ),

                                  ],
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                );
//                return new Card(
//                  child: new ListTile(
//                    onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
//                        builder: (BuildContext context) => SalonHomePage(
//                          salon_image: _userDetails[index].zurag,
//                          salon_name: _userDetails[index].cName,
//                          salon_address: _userDetails[index].hayg,
//                          salon_phone: _userDetails[index].utas,
//                        ))),
//
//                   leading: new CircleAvatar(backgroundImage: new NetworkImage('http://personal.psu.edu/xqz5228/jpg.jpg'),),
//                    title: new Text(_userDetails[index].cName + ' ' + _userDetails[index].hayg + '' + _userDetails[index].utas),
//                  ),
//                  margin: const E`dgeInsets.all(0.0),
//                );
              },
            ),
          ),
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _userDetails.forEach((userDetail) {
      if (userDetail.cName.contains(text) || userDetail.hayg.contains(text) || userDetail.utas.contains(text))
        _searchResult.add(userDetail);
    });

    setState(() {});
  }
}



List<UserDetails> _searchResult = [];

List<UserDetails> _userDetails = [];

final String url = 'http://192.168.2.110:9000/getCompanys';
class UserDetails {
  final int id;
  final String cName, hayg, utas, zurag;

  UserDetails({this.id, this.cName, this.hayg, this.utas, this.zurag });

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
        id: json['id'],
        cName: json['cName'],
        hayg: json['hayg'],
        utas: json['utas'],
        zurag: json['zurag']
    );
  }
}