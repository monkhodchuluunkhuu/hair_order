import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hair_order/homePages/homeScreen.dart';
import 'package:flutter_hair_order/json/uSignUpJson.dart';
import 'package:flutter_hair_order/userPages/signInPage.dart';

class User {

  String success ;
  String firstName;
  String lastName;
  String phone = '';
  String userName;
  String password ;
}

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) :super(key: key);
  final String title;

  @override
  _SignUpPageState createState() => new _SignUpPageState();

}

class _SignUpPageState  extends State<SignUpPage>{

  User newUser = new User();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController _controller = new TextEditingController();

  bool isValidPhoneNumber(String input) {
    final RegExp regex = new RegExp(r"^[0-9]");
    return regex.hasMatch(input);
  }

  void _submitForm() {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      showMessage('Формыг бүрэн бөглөнө үү!');
    } else {
      form.save(); //This invokes each onSaved event
      showDialog(context: context,
          builder: (  BuildContext context){
            return AlertDialog(
              title: Center(child: Text('Амжилттай бүртгэгдлээ')),
              content: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: <Widget>[
                  RaisedButton(
                    child: Text('Нэвтрэх'),
                    onPressed: (){
                      //_controller.clear();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()),);
                    },
                  ),
                  RaisedButton(
                    child: Text('Буцах'),
                    onPressed: (){
                      _controller.clear();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()),);
                    },
                  )

                ],),
            );
          }
      );

      var contactService = new UserService();
      contactService.createUser(newUser);
      //showMessage('New contact created for ${value.lastName}!', Colors.blue)
      showMessage('New contact created for ${newUser.userName}!', Colors.blue);

    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor: color, content: new Text(message)));
  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold
      (
      key: _scaffoldKey,
      appBar: new AppBar(
        title: Text('Бүртгүүлэх'),
      ),

      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: _formKey,
          autovalidate: true,
          child: new ListView
            (
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            children: <Widget>[
              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.perm_identity),
                  hintText: 'Овгоо оруулна уу',
                  labelText: 'Овог',
                ),
                validator: (val) => val.isEmpty ? 'Та өөрийн овгоо оруулах шаардлагатай!' : null,
                onSaved: (val) => newUser.firstName = val, ),

              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.person),
                  hintText: 'Нэр ээ оруулна уу',
                  labelText: 'Таны нэр',
                ),
                validator: (val) => val.isEmpty ? 'Та өөрийн нэрээ оруулах шаардлагатай!' : null,
                onSaved: (val) => newUser.lastName = val, ),

              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.phone),
                  hintText: 'Утасны дугаараа оруулна уу',
                  labelText: 'Утас',
                ),
                keyboardType: TextInputType.phone,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  new WhitelistingTextInputFormatter(new RegExp(r'^[()\d -]{1,15}$')),
                ],
                validator: (value) => isValidPhoneNumber(value) ? null : 'Холбоо барих утасны дугаар оруулах шаардлагатай!',
                onSaved: (val) => newUser.phone = val, ),

              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.account_circle),
                  hintText: 'Нэвтрэх нэр ээ оруулна уу',
                  labelText: 'Нэвтрэх нэр',
                ),
                validator: (val) => val.isEmpty ? 'Нэвтрэх нэрээ оруулах шаардлагатай!' : null,
                onSaved: (val) => newUser.userName = val, ),


              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.lock),
                  hintText: 'нууц үг оруулна уу',
                  labelText: 'Нууц үг',
                ),
                validator: (val) => val.isEmpty ? 'нэвтрэх нууц үг оруулах шаардлагатай!' : null,
                onSaved: (val) => newUser.password = val,),

              new Container(
                  padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                  child: new RaisedButton(
                    child: const Text('Бүртгэл'),
                    onPressed: _submitForm,
                  )),

            ],
          ),
        ),

      ),
    );
  }


}



