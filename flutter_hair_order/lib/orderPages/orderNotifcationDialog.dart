import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OrderRequest {
  final int orderId;
  final int barbarId;
  final String DateTime;

  OrderRequest({this.orderId, this.barbarId, this.DateTime});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["orderId"] = orderId;
    map["barbarId"] = barbarId;
    map["DateTime"] = DateTime;

    return map;
  }
}

createPost(String url, {Map body}) async {
  return http.post(url, body: json.encode(body)).then((http.Response response) {
    Map data;
    final int statusCode = response.statusCode;
    print(statusCode);
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    data = json.decode(response.body);
    print(data["success"]);
    return data["success"];
  });
}

class Order {
  final int orderId;
  final String orderCreateDate;
  final int orderType;
  final String productName;
  final int productPrice;

  Order(this.orderId, this.orderCreateDate, this.orderType, this.productName, this.productPrice);
}
Future<List<Order>> _getOrder() async {
  final data = await http.get("http://192.168.2.106:9000/getUschin");

  var jsonData = json.decode(data.body);

  List<Order> orders = [];

  for (var u in jsonData) {
    Order order =
    Order(u["orderId"], u["orderCreateDate"], u["orderType"], u["productName"], u["productPrice"]);
    orders.add(order);
  }

//  data = json.decode(response.body);
//  print(data["success"]);
//  return data["success"];
  return orders;
}

// Цаг сонголт, Цаг-Минут
//Future<TimeOfDay> _selectTime(BuildContext context,
//    {@required DateTime initialDate}) {
//  final now = DateTime.now();
//
//  return showTimePicker(
//    context: context,
//    initialTime: TimeOfDay(hour: initialDate.hour, minute: initialDate.minute),
//  );
//}

// Календар, Огноо-Сар-Өдөр
Future<DateTime> _selectDateTime(BuildContext context,
    {@required DateTime initialDate}) {
  final now = DateTime.now();
  final newestDate = initialDate.isAfter(now) ? initialDate : now;

  return showDatePicker(
    context: context,
    initialDate: newestDate.add(Duration(seconds: 1)),
    firstDate: now,
    lastDate: DateTime(2100),
  );
}

Dialog showDateTimeDialog(BuildContext context,
    {@required ValueChanged<DateTime> onSelectedDate,
      @required DateTime initialDate,
      @required int Type,
      @required String ServiceName,
      @required int BarberId,
    }) {
  final dialog = Dialog(
    child: DateTimeDialog(
      onSelectedDate: onSelectedDate,
      initialDate: initialDate,
      type: Type,
      serviceName: ServiceName,
      BarberId: BarberId,
    ),
  );

  showDialog(context: context, builder: (BuildContext context) => dialog);
}

/*
*  DateTimeDialog -> _DateTimeDialogState - Цаг сонгор үеийн харагдацыг дууддаг классууд.
*/
class DateTimeDialog extends StatefulWidget {
  final ValueChanged<DateTime> onSelectedDate;
  final DateTime initialDate;
  final int type;
  final String serviceName;
  final int BarberId;

  const DateTimeDialog({
    @required this.onSelectedDate,
    @required this.initialDate,
    @required this.type,
    @required this.serviceName,
    @required this.BarberId,
    Key key,
  }) : super(key: key);

  @override
  _DateTimeDialogState createState() => _DateTimeDialogState();
}

class _DateTimeDialogState extends State<DateTimeDialog> {
  DateTime selectedDate;
  static final CREATE_POST_URL = 'http://192.168.0.104:9000/loginAPI';
  var timeList = [
    '09:00',
    '09:30',
    '10:00',
    '10:30',
    '11:00',
    '11:30',
    '12:00',
    '12:30',
    '14:00',
    '14:30',
    '15:00',
    '15:30',
    '16:00',
    '16:30',
    '17:00',
    '17:30',
  ];
  var timeItemSelected = '09:00';
  var endTime = '';
  var startTime = '';

  @override
  void initState() {
    super.initState();

    selectedDate = widget.initialDate;
  }

  String dropDownItemSelected(String newValueSelected) {
    setState(() {
      this.timeItemSelected = newValueSelected;
    });
    String time = this.timeItemSelected;
    print("Time: " + time); // Time: 11:00

    var curDate;
    var a = time.split(":");
    var myMinute = int.parse(a[1]) + widget.type;

    if(myMinute >= 60) {
      curDate = (int.parse(a[0]) + 1).toString() + ":" + (myMinute -60).toString();
    } else {
      curDate = a[0] + ":" + (myMinute).toString();
    }
    print(curDate);
    return curDate;
  }
  String starTime(String newValueSelected) {
    setState(() {
      this.timeItemSelected = newValueSelected;
    });
    String time = this.timeItemSelected;
    print("Time: " + time); // Time: 11:00
    return time;
  }

  @override
  Widget build(BuildContext context) => Padding(
    padding: const EdgeInsets.all(16),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          'Өдөр',
          style: Theme.of(context).textTheme.title,
        ),
        const SizedBox(height: 16),
        RaisedButton(
          highlightColor: Colors.deepOrange,
          child: Text(DateFormat('yyyy-MM-dd').format(selectedDate)),
          onPressed: () async {
            final date =
            await _selectDateTime(context, initialDate: selectedDate);
            if (date == null) return;

            setState(() {
              selectedDate = DateTime(
                date.year,
                date.month,
                date.day,
                selectedDate.hour,
                selectedDate.minute,
              );
            });

            widget.onSelectedDate(selectedDate);
          },
        ),

        const SizedBox(height: 16),
        Text(
          'Эхлэх цаг',
          style: Theme.of(context).textTheme.title,
        ),
        DropdownButton<String>(
          items: timeList.map((String dropDownStringItem) {
            return DropdownMenuItem<String>(
              value: dropDownStringItem,
              child: Text(dropDownStringItem),
            );
          }).toList(),
          onChanged: (String newValueSelected) {
            print(newValueSelected);
            var result = dropDownItemSelected(newValueSelected);
            var startTimeResult = starTime(newValueSelected);
            print("createPost return = ${result}");
            endTime = result;
            startTime = startTimeResult;
            print("End Time: "+endTime);
            print("Start Time: "+startTime);
          },
          value: timeItemSelected,
        ),

        const SizedBox(height: 16),
        Text(endTime, style: Theme.of(context).textTheme.title,),

        const SizedBox(height: 16),
        FutureBuilder(
          future: _getOrder(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print("end orj ir");
            if (snapshot.data == null) {
              return Container(
                child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(backgroundColor: Colors.deepOrange,),
                      ],
                    )),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return OutlineButton(
                    child: Text('Баталгаажуулах'),
                    onPressed: () async {
                      OrderRequest newPost = new OrderRequest(
                          orderId: snapshot.data[index].orderId,
                          barbarId: widget.BarberId,
                          DateTime: startTime
                      );
                      var body =
                      await createPost(CREATE_POST_URL, body: newPost.toMap());
                      print("createPost return = ${body}");
                      if (body == true) {
                        print("Amjilttai newterlee.");

                      } else {
                        print("Bolomjgvi!!!");
                      }
                    },
                    highlightColor: Colors.deepOrange,
                  );
                },
              );
            }
          },
        ),

      ],
    ),
  );
}
