/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';

/*
*  ProductList - Бүтээгдэхүүний ангиллыг харах хэсгийн логонуудын жагсаалтыг дууддаг класс
*/
class ProductList extends StatelessWidget {

  // @return -> Container буцаана(Энэ нь дотороо ListView агуулсан ба тэр дотороо Category гэсэн классыг дуудна).
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
            image_location: 'image/salonCategory/razor.png',
            image_caption: 'Сахалын хөөс',
          ),
          Category(
            image_location: 'image/salonCategory/moisturizer.png',
            image_caption: 'Нүүрний тос',
          ),
          Category(
            image_location: 'image/salonCategory/hairdryer.png',
            image_caption: 'Үсний сэнс',
          ),
          Category(
            image_location: 'image/salonCategory/razor.png',
            image_caption: 'Сахалын хөөс',
          ),
          Category(
            image_location: 'image/salonCategory/moisturizer.png',
            image_caption: 'Нүүрний тос',
          ),
          Category(
            image_location: 'image/salonCategory/hairdryer.png',
            image_caption: 'Үсний сэнс',
          ),
        ],
      ),
    );
  }
}

/*
*  ProductList - Бүтээгдэхүүний ангиллын харагдацыг дууддаг класс
*  @param - image_location(бүтээгдэхүүний лого), image_caption(бүтээгдэхүүний нэр) гэсэн хоёр утга авна.
*/
class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({
    this.image_location,
    this.image_caption,
  });

  // @return -> Padding буцаана(Энэ Padding нь нэг хэсгийн жагсаалтын харагдацыг агуулсан).
  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(2.0),
      child: InkWell(onTap: (){},
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(image_location, height: 60.0,),
            subtitle: Text(image_caption, style: TextStyle(fontSize: 12.0, color: Colors.black),),
          ),
        ),
      ),
    );
  }
}

