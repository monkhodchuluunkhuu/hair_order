/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/orderPages/orderBarberList.dart';
import 'package:flutter_hair_order/orderPages/orderServiceName.dart';

/*
*  OrderHome -> _OrderHomeState - Захиалга өгөх үндсэн хуудсыг дууддаг класс
*/
class OrderHome extends StatefulWidget {
  final String ServiceName;
  final int ServiceDuraction;

  //  @param - orderPages -> serviceName.dart-аас үйлчилгээний нэрийг авна.
  OrderHome({
    this.ServiceName,
    this.ServiceDuraction
  });

  @override
  _OrderHomeState createState() => _OrderHomeState();
}

class _OrderHomeState extends State<OrderHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.deepOrange,
        title: Text(
            "Цаг захиалга"),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Үйлчилгээний нэр ',
              style:
              TextStyle(fontWeight: FontWeight.bold, color: Colors.black45),
            ),
          ),

          // Үйлчилгээний нэр
          OrderServiceName(
            serviceName: widget.ServiceName,
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Үсчний жагсаалт',
              style:
              TextStyle(fontWeight: FontWeight.bold, color: Colors.black45),
            ),
          ),

          // Үсчингүүдийн жагсаалт
          Container(
            height: 320.0,
            child: OrderBarberList(
                ServiceDuraction: widget.ServiceDuraction,
                ServiceName: widget.ServiceName
            ),
          ),

        ],
      ),
    );
  }
}
