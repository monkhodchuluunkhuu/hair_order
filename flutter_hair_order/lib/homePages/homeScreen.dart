import 'package:flutter/material.dart';
import 'package:flutter_hair_order/homePages/salonPage.dart';
import 'package:flutter_hair_order/json/salons.json.dart';
import 'package:flutter_hair_order/userPages/signInPage.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'homePageShape.dart';

var viewAllStyle = TextStyle(fontSize: 16.0, color: Colors.black);

Future<List<Salon>> _getSalons() async {
  final data =
  await http.get("http://192.168.2.110:9000/getCompanys");
  var jsonData = json.decode(data.body);

  List<Salon> users = [];

  for (var u in jsonData) {
    Salon user = Salon(u["cId"], u["cName"], u["hayg"], u["utas"], u["zurag"]);
    users.add(user);
  }
  print(users.length);
  return users;
}



class HomeScreen extends StatefulWidget{
  @override
  homeScreenState createState() => homeScreenState();
}

class homeScreenState extends State<HomeScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,

        child: Column(
          children: <Widget>[

            HomeScreenTop(),

//            SizedBox(height:10.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Салон жагсаалт", style: TextStyle(fontSize: 30.0,  fontStyle: FontStyle.normal))],
              ),//Row
            ),//Padding

//            SizedBox(height: 10.0),
            Container(
              height: 500.0,
              width: 500.0,

              child: FutureBuilder(
                future: _getSalons(),
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  if (snapshot.data == null)
                  { return Container(
                    child: Center(
                      child: Text(" Loading..."),),);}//if
                  else {
                    return ListView.builder(  itemCount: snapshot.data.length, itemBuilder: (BuildContext context, int index){
                      return GestureDetector(
                        onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) => SalonHomePage(
                              salon_image: snapshot.data[index].zurag,
                              salon_name: snapshot.data[index].cName,
                              salon_address: snapshot.data[index].hayg,
                              salon_phone: snapshot.data[index].utas,
                              company_id: snapshot.data[index].cId,
                            ))),
                        child: new Card(
                          child: new Column(
                            children: <Widget>[
                              new Container(
                                width: 400,
                                height: 200,
                                decoration: BoxDecoration(
                                  image: new DecorationImage(image: new NetworkImage(snapshot.data[index].zurag ),
                                      fit: BoxFit.cover
                                  ),
                                ),
                              ),
                              new Column(
                                children: <Widget>[
                                  new Row(
                                    children: <Widget>[
                                      SizedBox(height: 10.0),
                                      new Container(
                                        color: Colors.deepPurple[50],
                                        width: 150.0,
                                        height: 50.0,
                                        child: new Text(snapshot.data[index].cName, style: TextStyle(fontSize: 20, color: Colors.black, fontStyle: FontStyle.normal),),

                                      ),
                                      SizedBox(width: 10.0,),
                                      new Row(
                                        children: <Widget>[
                                          new Column(
                                            children: <Widget>[
                                              new Text(snapshot.data[index].hayg, style: TextStyle(fontSize: 14.0, color: Colors.black, fontStyle: FontStyle.normal),),
                                              SizedBox(height: 5.0,),
                                              new Text(snapshot.data[index].utas, style: TextStyle(fontSize: 14.0, color: Colors.black, fontStyle: FontStyle.normal),),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }
                    );
                  }
                },
              ),
            )
          ],//Children Widget
        ),
      ),

    );
  }

}

class HomeScreenTop  extends StatefulWidget{
  @override
  HomeScreenTopState createState() =>  HomeScreenTopState ();
}

class HomeScreenTopState extends State<HomeScreenTop> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(

      children: <Widget>[
        ClipPath(clipper: HomePageShape(),
          child: Container(height: 300.0, width: 450, color: Colors.orange,
            // BoxDecoration

            child: Column(
              children: <Widget>[
                SizedBox(height: 30.0),

                Padding(
                  padding: EdgeInsets.only( left: 240,right: 10.0),
                  child: FlatButton.icon(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()),);},

                      icon: Icon(Icons.account_circle), label: new Text("Миний бүртгэл")),
                ),
                SizedBox(height: 80.0,),
                Text('BARBER SHOPS', style: TextStyle(fontSize: 30.0, color: Colors.black), textAlign: TextAlign.center,),
                SizedBox(height: 30.0,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal:32.0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  ),//Material
                )//Padding
              ],//Widget
            ),//Column
          ), // Container
        )//ClipPaht

      ],//Widget
    );//Stack
  }
}

