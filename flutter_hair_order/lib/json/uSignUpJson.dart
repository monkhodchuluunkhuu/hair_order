
import 'package:flutter_hair_order/userPages/singUpPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';





class UserService {
  static const _serviceUrl = 'http://192.168.2.110:9000/signin';
  static final _headers = {'Content-Type': 'application/json'};

  Future<User> createUser(User user) async {
    try {
      String json = _toJson(user);
      final response =
      await http.post(_serviceUrl, headers: _headers, body: json);
      var c = _fromJson(response.body);
      return c;
    } catch (e) {
      print('Server Exception!!!');
      print(e);
      return null;
    }
  }

  User _fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    var user = new User();

    user.firstName = map['firstName'];
    user.lastName = map['lastName'];
    user.phone = map['phone'];
    user.userName =  map['userName'];
    user.password = map['password'];

    return user;
  }

  String _toJson(User user) {
    var mapData = new Map();
    mapData["firstName"] = user.firstName;
    mapData["lastName"] = user.lastName;
    mapData["phone"] = user.phone;
    mapData["userName"] = user.userName;
    mapData["password"] = user.password;
    String json = jsonEncode(mapData);
    return json;
  }
}