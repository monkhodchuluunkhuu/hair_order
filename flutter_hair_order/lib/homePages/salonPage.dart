import 'package:flutter/material.dart';
import 'package:flutter_hair_order/servicePages/serviceType.dart';
import 'package:flutter_hair_order/produtsPages/proSalonHome.dart';
import 'dart:convert';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SalonHomePage extends StatefulWidget {

  final salon_name;
  final salon_image;
  final salon_address;
  final salon_phone;
  final company_id;

  SalonHomePage({
    this.salon_name,
    this.salon_image,
    this.salon_address,
    this.salon_phone,
    this.company_id
  });


  @override
  _SalonHomePageState createState() => _SalonHomePageState();
}

class _SalonHomePageState extends State<SalonHomePage> {

  PageController _pageController;
  int _page = 0;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void navigationTapped(int page) {
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }


  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      bottomNavigationBar: BottomNavigationBar(

        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.black,),
            title: Text("Үндсэн хуудас",style: TextStyle(color: Colors.black),),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.brightness_high, color: Colors.black,),
            title: Text('Үйлчилгээ',style: TextStyle(color: Colors.black),),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.featured_play_list, color: Colors.black,),
            title: Text('Бараа',style: TextStyle(color: Colors.black),),
          ),
        ],
        onTap: navigationTapped,
        currentIndex: _page,
      ),
      appBar:AppBar(
        elevation: 0.1,
        backgroundColor: Colors.deepOrange,
        title: Text(widget.salon_name, style: TextStyle(color: Colors.white)),
      ),
      body: new PageView(
        children: [

          new ListView(
            children: <Widget>[
              Container(
                height: 300.0,

                child: GridTile(

                  child: Container(
                    color: Colors.white,
                    child: Image.network(widget.salon_image),
                    //padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  ),

                  footer: Container(
                    color: Colors.deepPurple[50],
                    child: ListTile(
                      title: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(widget.salon_address, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),),
                          ),
                          Expanded(
                            child: Text(widget.salon_phone, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          ServiceType(companyId: widget.company_id,),
          SalonProducts()
          //new Friends("Friends screen"),
        ],

        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
    );
  }
}

