/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';

/*
*  OrderHome -> _ServiceNameState - Үйлчилгээний нэрийг авдаг класс
*/
class OrderServiceName extends StatefulWidget {
  final serviceName;

  OrderServiceName({
    this.serviceName,
  });
  @override
  _OrderServiceNameState createState() => _OrderServiceNameState();
}

class _OrderServiceNameState extends State<OrderServiceName> {

  //  @param - servicePages.dart-аас үйлчилгээний нэрийг авна.
  //  @return -> Container буцаана(Энэ Container нь үйлчилгээний нэрийн харагдацыг).
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 5.0),
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        child: Row(
          children: <Widget>[
            InkWell(
              splashColor: Colors.deepOrange,
              onTap: () =>{},
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  widget.serviceName,
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
