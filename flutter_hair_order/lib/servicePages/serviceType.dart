/*
* Creade by Chuluunkhuu
* Date: 6/21/2019
*/
import 'package:flutter/material.dart';
import 'package:flutter_hair_order/servicePages/serviceList.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Category {
  final int CategoryId;
  final String CategoryName;
  final int categoryCompany;

  Category(this.CategoryId, this.CategoryName, this.categoryCompany);
}


/*
*  ServiceType -> _ServiceTypeState - Үйлчилгээ харах, сонгох хэсгийн үндсэн хуудсыг дууддаг класс
*/
class ServiceType extends StatefulWidget {
  final companyId;
  ServiceType({
    this.companyId,
  });
  @override
  _ServiceTypeState createState() => _ServiceTypeState(companyId: companyId);
}

class _ServiceTypeState extends State<ServiceType> {
  final companyId;
  _ServiceTypeState({
    this.companyId,
  });

  Future<List<Category>> _getCategory() async {
    final data = await http.get("http://192.168.2.110:9000/getCategorys/${companyId}");

    var jsonData = json.decode(data.body);

    List<Category> categorys = [];

    for (var u in jsonData) {
      Category user =
      Category(u["CategoryId"], u["CategoryName"], u["categoryCompany"]);
      categorys.add(user);
    }
    return categorys;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder(
          future: _getCategory(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(backgroundColor: Colors.deepOrange,),
                  ],
                )),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ServiceList(
                                  ServiceName: snapshot.data[index].CategoryName,
                                  ServiceTypeID: snapshot.data[index].CategoryId,
                                ))),
                    child: Container(
                      height: 60.0,
                      width: 60.0,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                      child: Card(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      snapshot.data[index].CategoryName,
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}

//class ListToturial extends StatelessWidget {
//  final String ServiceTypeName;
//
//  /*
//  *  ListToturial - классын байгуулагч функц
//  *  @param - title(үйлчилгээний нэр), explanation(тухайн үйлчилгээ бүрт байх тайлбарууд) гэсэн хоёр утга авна.
//  */
//  ListToturial({
//    this.ServiceTypeName,
//  });
//
//  // @return -> Container буцаана(Энэ Container нь нэг хэсгийн жагсаалтын харагдацыг агуулсан).
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: 60.0,
//      width: 60.0,
//      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
//      child: Card(
//        color: Colors.white,
//        shape:
//        RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: <Widget>[
//            Row(
//              children: <Widget>[
//                InkWell(
//                  splashColor: Colors.deepOrange,
//                  onTap: () => {},
//                  child: Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Text(
//                      ServiceTypeName,
//                      style: TextStyle(
//                        fontSize: 15.0,
//                        color: Colors.black,
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//
//            // button дээр дарахад OrderHome буюу Захиалга гэсэн хуудас руу үсэрнэ. Тус хуудас руу үйлчилгээнийхээ нэрийг дамжуулна.
//            IconButton(
//              icon: new Icon(
//                Icons.arrow_forward_ios,
//                color: Colors.deepOrange,
//              ),
//              onPressed: () => Navigator.of(context).push(new MaterialPageRoute(
//                  builder: (BuildContext context) => ServiceList(
//                    ServiceTypeName: ServiceTypeName,
//                  ))),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
